#!/bin/sh
TARGET_BRANCH=$1
shift 1
FEATURE_BRANCH=$1
shift 1
SF_SERVERURL=$1
shift 1
SF_USERNAME=$1
shift 1
SF_PASSWORD=$1

git fetch
git checkout $TARGET_BRANCH
git pull
git checkout -b $FEATURE_BRANCH origin/$FEATURE_BRANCH
git checkout $TARGET_BRANCH
git merge --no-ff $FEATURE_BRANCH
mergeResult=$?
if [ $mergeResult -gt 0 ]; then
	exit $mergeResult
fi

rm -fr deploy/$FEATURE_BRANCH/$TARGET_BRANCH
sfpackage origin/$TARGET_BRANCH $TARGET_BRANCH ./deploy/$FEATURE_BRANCH

## Prepare build package
# sh prepareBuild.sh ./deploy/$FEATURE_BRANCH/$TARGET_BRANCH
SF_PACKAGE=./deploy/$FEATURE_BRANCH/$TARGET_BRANCH
echo Construyendo build.ci.xml para paquete $SF_PACKAGE
CWD=`pwd`
cd $SF_PACKAGE
grep -ri "@isTest\|testmethod" unpackaged/classes/* | cut -d: -f1 | cut -d/ -f3 | sort -u > $CWD/testsFiles.tmp
ls unpackaged/classes
EXISTS_CLASSES=$?
ls unpackaged/triggers
EXISTS_TRIGGERS=$?

sed -i 's/\(\w*\)\(.cls\)/\<runTest\>\1<\/runTest\>/g' $CWD/testsFiles.tmp
testing=`sed -r 's:[\/]+:/:g' $CWD/testsFiles.tmp`
testing=`echo $testing`
echo "sed 's:{{TEST_TO_EXECUTE}}:$testing:g' $CWD/build.xml.template > $CWD/build.ci.xml" | sh

if [ -z "${CI_JOB_ID}" ]; then
    echo "sed -i 's:<\/runTest>:<\/runTest>\\n:g' $CWD/build.ci.xml" | sh
else
    echo "sed -i 's:<\/runTest>:<\/runTest>\\\\n:g' $CWD/build.ci.xml" | sh
fi

cd $CWD
## Select ant task
echo $EXISTS_CLASSES "Classes"
echo $EXISTS_TRIGGERS "Triggers" 
targetTask=deployUnpackagedDefaultTest
if [ -s testsFiles.tmp -o $EXISTS_CLASSES -eq 0 -o $EXISTS_TRIGGERS -eq 0 ]; then
	targetTask=deployUnpackaged
fi
echo Running ANT Tast:  $targetTask
echo Wroking directory: $CWD
echo Buildfile:         build.ci.properties
echo Package:           deploy/$FEATURE_BRANCH/$TARGET_BRANCH
ant -f build.ci.xml -propertyfile build.ci.properties -Dsf.serverurl=$SF_SERVERURL -Dsf.username=$SF_USERNAME -Dsf.password=$SF_PASSWORD -Dsf.package=deploy/$FEATURE_BRANCH/$TARGET_BRANCH $targetTask 
exit $?


