import { LightningElement, api } from 'lwc';

export default class DemoSecondChild extends LightningElement {

	constructor() {
		super();
		if (!this.case) {
			this.myCase = { case: {} };
		}
	}

	//executing from father, make click in hidden submit button in Child 1
	@api
	validateFields() {
		var submitBtn = Array.from(this.template.querySelectorAll('lightning-button'))
			.filter(element => element.title === "save_CaseChild_2")[0];
		window.console.log("Validating Second Child...");

		submitBtn.click();
	}

	saveCaseSecondChild(event) {
		window.console.log("Submit button in second child clicked, saving Second Child...");
		event.preventDefault();
		const fields = event.detail.fields;
		this.myCase.case.fields = fields;

		//Send case to father
		const evt = new CustomEvent('submitdatatwo', { detail: JSON.stringify(this.myCase) });
		this.dispatchEvent(evt);
	}
}