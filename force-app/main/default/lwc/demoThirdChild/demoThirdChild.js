import { LightningElement, api } from 'lwc';

export default class DemoThirdChild extends LightningElement {

	constructor() {
		super();
		if (!this.case) {
			this.myCase = { case: {} };
		}
	}

	//executing from father, make click in hidden submit button in Child 3
	@api
	validateFields() {
		var submitBtn = Array.from(this.template.querySelectorAll('lightning-button'))
			.filter(element => element.title === "save_CaseChild_3")[0];
		window.console.log("Validating Third Child...");

		submitBtn.click();
	}

	saveCaseThirdChild(event) {
		window.console.log("Submit button in third child clicked, saving Third Child...");
		event.preventDefault();
		const fields = event.detail.fields;
		this.myCase.case.fields = fields;

		//Send case to father
		const evt = new CustomEvent('submitdatathree', { detail: JSON.stringify(this.myCase) });
		this.dispatchEvent(evt);
	}
}