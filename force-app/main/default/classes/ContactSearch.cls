public class ContactSearch {
    public static List<Contact> searchForContacts(String lastName, String zipCode){
		List<Contact> l = new List<Contact>();
        l=[SELECT Id,Name FROM Contact WHERE LastName= :lastName 
           AND MailingPostalCode= :zipCode];
		return l;        
    }
}