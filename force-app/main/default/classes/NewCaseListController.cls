public class NewCaseListController {
    public List<Case> getNewCases(){
        List<Case> lc = [SELECT CaseNumber FROM Case WHERE Status='New'];
        return lc;
    }
}