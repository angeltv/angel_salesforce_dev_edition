global class DailyLeadProcessor implements Schedulable{
    global void execute(SchedulableContext context){
        //search leads with LeadSource =null
        List<lead> leads = [SELECT Id, LeadSource FROM Lead WHERE LeadSource =null];
        
        //Edit LeadSource field in every Lead
        if(!leads.isEmpty()){
            for(Lead l:leads){
                l.LeadSource = 'Dreamforce';
            }
            update leads;
        }
    }
}