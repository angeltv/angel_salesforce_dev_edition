@isTest
public class DailyLeadProcessorTest {
    
    private static final Integer NUM_RECORDS = 200;
    
    @isTest static void TestDailyLeadProcessor(){
        String sch = '00 40 10 15 05 ? 2017';
        List<Lead> leads = new List<Lead>();
        for(Integer i=0; i<NUM_RECORDS; i++){
            String s = 'Dreamforce '+i;
            Lead l = new Lead(FirstName=s, LastName='TestDailyLeadProcessor', Company='Urifrisky',Leadsource = null);
            leads.add(l);  
        }
        
        insert leads;
        Test.startTest();
        String jobId = System.schedule('Daily Processor Test', sch, new DailyLeadProcessor());
        Test.stopTest();
    }
}