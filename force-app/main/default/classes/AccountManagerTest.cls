@isTest
public class AccountManagerTest {
	@isTest static void testGetAccount() {
        Id recordId = createTestRecord();
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://eu11.salesforce.com/services/apexrest/Accounts/'
            + recordId + '/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        // Call the method to test
        Account thisAccount = AccountManager.getAccount();
        // Verify results
        System.assert(thisAccount != null);
        System.assertEquals('Test record', thisAccount.Name);
    }
    
    //HelperMethod that creates a new Account with a new contact.
    static Id createTestRecord(){
        //create test record
        Account testAcc = new Account(Name= 'Test record');
        insert testAcc;
        Contact testCon = new Contact(LastName='Test', AccountId=testAcc.id);
        return testAcc.id;
    }
}