public class AccountProcessor {
	@future
    public static void countContacts(List<id> ids){
        //fins acounts
        List<Account> l = [SELECT id, Number_of_Contacts__c, (SELECT id FROM Contacts) FROM account WHERE id IN :ids];
        for(Account acc:l){
            List<Contact> lstContact = acc.contacts;
            acc.Number_of_Contacts__c = lstContact.size();
        }
        update l;
    }
}