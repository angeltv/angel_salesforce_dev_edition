@isTest
private class LeadProcessorTest {
    @isTest static void leadTest(){
        //create new leads
        String n;
        List<Lead> leads = new List<Lead>();
        for (Integer i = 0; i < 200; i++) {
            Lead l = new Lead();
            l.FirstName = 'FirstName';
            l.LastName ='LastName'+i;
            l.Company ='demo'+i;

            leads.add(l);
            
        }
        insert leads;
		//test LeadProcessor
        Test.startTest();
        	DataBase.executeBatch(new LeadProcessor());
        Test.stopTest();
    }
}