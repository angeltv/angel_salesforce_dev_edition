@isTest
public class AddPrimaryContactTest {
    @isTest static void testList(){
        
        //create and insert Accounts
        List<Account> la = new List<Account>();
        for(Integer i=0;i<50;i++){
            la.add(new Account(BillingState='CA', name='Test'+i));
        }
        
        for(Integer j=0; j<50;j++){
            la.add(new Account(BillingState='NY', name='Test'+j));
        }
        
        insert la;
        
        //create AddPrimaryContact
        Contact c = new Contact();
        c.FirstName='urifrisky';
        c.LastName ='uri';
        insert c;
        String sA = 'CA';
        AddPrimaryContact apc = new AddPrimaryContact(c, Sa);
        
        //execute test
        Test.startTest();
        System.enqueueJob(apc);
        Test.stopTest();
    }
}