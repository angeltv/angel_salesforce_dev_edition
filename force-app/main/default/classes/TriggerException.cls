public class TriggerException extends Exception {

	private Integer code {get;set;}
	private String 	message {get;set;}

	public TriggerException(Integer code, String message){
		this.message = message;
		this.code = code;		
	}
}