public class RandomContactFactory {
    public static List<Contact> generateRandomContacts(Integer numContacts, String s){
        List<Contact> lc = new List<Contact>();
        Contact c;
        String fn;
        for (Integer i =0; i<numContacts;i++){
            fn=s+i;
            c=new Contact(FirstName=fn);
            lc.add(c);
        }
        return lc;
    }
}