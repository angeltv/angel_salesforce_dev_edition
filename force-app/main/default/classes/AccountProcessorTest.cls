@isTest
public class AccountProcessorTest {
    @isTest static void testAccountProcessor(){
        
        //create test account
        Account a = new Account(Name='Test AccountPi');
        insert a;
        
       	//add Contact to test account
       	Contact c = new Contact(FirstName = 'Bob', LastName = 'Esponja', AccountId = a.id);
        insert c;
        
        //Test
        List<id> ids = new List<id>();
        ids.add(a.id);
        Test.startTest();
        	AccountProcessor.countContacts(ids);
        Test.stopTest();
        
    }
}