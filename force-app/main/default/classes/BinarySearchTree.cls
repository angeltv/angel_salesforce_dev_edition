/**
 * @file       	BinarySearchTree.cls
 * @author  	Ángel Troya 
 * @date        01-05-2019  
 * @description Class Binary Search Tree.
 */
public with sharing class BinarySearchTree{

    private Node root;
    private Comparator comp;
    private Integer elementCount = 0;
    private Integer modCount = 0;

    public BinarySearchTree() {
        this.root = null;
    }

    public BinarySearchTree(Comparator c){
        if(c != null){
            this.comp = c;
        }
    }

    public boolean contains(SObject obj){
        boolean result = false;
        Node current = root;

        while (current != null && ! result){
            Integer cmp = compare(obj, current.info);
            if(cmp < 0){
                current = current.getchildLeft();
            }
            else if (cmp > 0){
                current = current.getChildRight();
            }
            else{
                result = true;
            }
        }

        return result;
    }

    //TODO Revise 
    private boolean containsNodeRecursive(Node current, SObject value) {
    if (current == null) {
        return false;
    }
    Integer comp = compare(value, current.getInfo()); 
    if (comp == 0) {
        return true;
    } 
    return comp < 0 ? containsNodeRecursive(current.getchildLeft(), value): containsNodeRecursive(current.getchildRight(), value);
    }

    //TODO
    public Integer compare(Object o1, Object o2){
        return 0;
    }

    public boolean add(Sobject info){
        if(info == null){
            throw new NullPointerException();
        }
        Boolean b = false;
        if(root == null){
            root = new Node(info);
            b = true;
            elementCount++;
            modCount++;
        }
        boolean dir = false;
        Node current = root;
        Node father = null;
        while(current != null && !b){
            Integer cmp = compare(info, current.info);
            if(cmp == 0){
                father = null;
                current = null;
            }
            else if(cmp > 0){
                father = current;
                current = current.getchildRight();
                dir = true;
            }
            else{
                father = current;
                current = current.getchildLeft();
                dir = false;
            }
        }
        if(father != null){
            b = true;
            elementCount++;
            modCount++;
            if(dir){
                father.leftChild = new Node(info);
            }
            else{
                father.rightChild = new Node(info);
            }
        }
        return b;
    }

    //TODO Revise left and right child is assigned ok 
    public Node addRecursive(Node current, Sobject value) {
        if (current == null) {
            return new Node(value);
        }
        Integer comp = compare(value, current.getInfo());
        if (comp < 0) {
            current.leftChild = addRecursive(current.getchildLeft(), value);
        } else if (comp > 0) {
            current.rightChild = addRecursive(current.getchildRight(), value);
        } else {
            // value already exists
            return current;
        }
        return current;
    }

    private class Node{

        private SObject info;
        private Node leftChild;
        private Node rightChild;

        public Node(SObject info) {
            this.info = info;
            leftChild = null;
            rightChild = null;
        }

        public SObject getInfo(){
            return info;
        }

        public Node getchildLeft(){
            return leftChild;
        }

        public Node getchildRight(){
            return rightChild;
        }

        //TODO
        /*public override SObject clone(boolean clonepreserveId, boolean isDeepClone, boolean preserveReadonlyTimestamps, boolean preserveAutonumber){
            //clone(preserveId, isDeepClone, preserveReadonlyTimestamps, preserveAutonumber)
            Node clon = (Node)super.clone(false, true, false, false);
            clon.info = this.info.clone(false, true, false, false);
            if(this.leftChild != null){
                clon.leftChild = (Node)this.leftChild.clone(false, true, false, false);
            }
            if(this.rightChild != null){
                clon.rightChild = (Node)this.rightChild.clone(false, true, false, false);
            }
            return clon;
        }*/
    }
}