public with sharing class AccountTriggerHandler implements ITrigger {
	
	
	private static Map<Id, Account> duplicados = new Map<Id,Account>();
	
	public AccountTriggerHandler() {
		
	}

	public void bulkBefore(){
		List<String> externalIds;
		List<Account> duplicados;
		List<Account> scope = (List<Account>)trigger.new;
   //     if (!trigger.isDelete) {
			//for (Account original : [SELECT Id, Name, Phone, Email__c, External_Id__c FROM Account
   //                 	WHERE External_Id__c IN :externalIds]) {
   //             duplicados.put(original.External_Id__c, original);
   //     	}    
   // 	}
	}

	public void bulkAfter(){
	
	}

	public void beforeInsert(SObject so){
		Account acc = (Account) so;
		acc.External_Id__c = acc.Phone + acc.DNI__c;
	}

	public void beforeUpdate(SObject so1 , SObject so2){

	}

	public void beforeDelete(SObject so){

	}

	public void afterInsert(SObject so){

	}

	public void afterUpdate(SObject so1, SObject so2){

	}

	public void afterDelete(SObject so){

	}

	public void andFinally(){

	}
	
}