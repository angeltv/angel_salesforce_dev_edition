@isTest 
public class TestVerifyDate {
    //test <=30 days
    @isTest static void testCheckDates(){
        Date date1 = Date.newInstance(2017,5,10);
        Date date2 = Date.newInstance(2017,5,20);
        Date dateSol = VerifyDate.CheckDates(date1, date2);
        System.assertEquals(date2, dateSol);
    }
    
    //test >=30 days
    @isTest static void testCheckDates2(){
        Date endOfMonth = Date.newInstance(2017,5,31);
        Date date1 = Date.newInstance(2017,5,10);
        Date date3 = Date.newInstance(2017,6,20);
        Date dateSol = VerifyDate.CheckDates(date1, date3);
        System.assertEquals(dateSol, endOfMonth);
        
    }
}