global class AddPrimaryContact implements Queueable{
    
    private Contact c;
    private String sA;
    
    public AddPrimaryContact(Contact ct, String stateAb){
        c = ct;
        sA = stateAb;
    }
    
     //NO HECHO POR MI, COMPRENDER COMO FUNCIONA, por la cuenta que me trae
    global void execute(QueueableContext qc){
        List<Account> ListAccount = [SELECT ID, Name ,(Select id,FirstName,LastName from contacts ) FROM ACCOUNT WHERE BillingState = :sA LIMIT 200];
         List<Contact> lstContact = new List<Contact>();
         for (Account acc:ListAccount)
         {
                 Contact cont = c.clone(false,false,false,false);
                 cont.AccountId =  acc.id;
                 lstContact.add( cont );
         }
         
         if(lstContact.size() >0 )
         {
             insert lstContact;
         }
    }

}