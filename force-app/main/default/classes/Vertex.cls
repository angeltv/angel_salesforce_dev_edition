/**
 * @file       	Vertex.cls
 * @author  	Ángel Troya 
 * @date        01-05-2019  
 * @description Class Vertex for Graph.
 */
public with sharing class Vertex {

    //hashSet
    Set<Vertex> edges;

    public Vertex() {
        edges = new Set<Vertex>();
    }
}