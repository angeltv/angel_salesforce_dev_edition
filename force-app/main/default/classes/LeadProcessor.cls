global class LeadProcessor implements Database.Batchable<sObject>{
    global Database.QueryLocator start (Database.BatchableContext bc){
        return Database.getQueryLocator('SELECT Name, leadSource FROM Lead'); 
    }
    
    global void execute (Database.BatchableContext bc, List<Lead> records){
		if(!records.isEmpty()){
            for(Lead l: records) {
                l.LeadSource = 'Dreamforce';
            }
            update records;
        }
    }
    
    global void finish (Database.BatchableContext bc){
        
    }
}