trigger ClosedOpportunityTrigger on Opportunity (before insert, before update) {
    List<Task> taskList = new List<Task>();
    Task t;
    for(Opportunity opp: Trigger.new){
        //if new Opportunity
        if(Trigger.isInsert){
            if(opp.StageName=='Closed Won'){
                //create new Task
                t= new Task(Subject='Follow Up Test Task', WhatId = opp.Id);
                taskList.add(t);
            }
        }
        //if update Opportunity
        if(Trigger.isUpdate){
            //if StageName is changing in this update record.
            if(opp.StageName=='Closed Won' 
               && opp.StageName!=Trigger.oldMap.get(opp.Id).Stagename){
                   	t= new Task(Subject='Follow Up Test Task', WhatId = opp.Id);
                	taskList.add(t);
           	}
        }
    }
    //insert tasks
    if(taskList.size()>0){
        insert taskList;
    }
}