#!/bin/sh
SF_PACKAGE=$1
echo Construyendo build.ci.xml para paquete $SF_PACKAGE
CWD=`pwd`
cd $SF_PACKAGE
grep -ri "@isTest\|testmethod" unpackaged/classes/* | cut -d: -f1 | cut -d/ -f3 | sort -u > $CWD/testFiles.tmp
sed -i 's/\(\w*\)\(.cls\)/\<runTest\>\1<\/runTest\>/g' $CWD/testFiles.tmp
testing=`sed -r 's:[\/]+:/:g' $CWD/testFiles.tmp`
testing=`echo $testing`
echo "sed 's:{{TEST_TO_EXECUTE}}:$testing:g' $CWD/build.xml.template > $CWD/build.ci.xml" | sh

if [ -z "${CI_JOB_ID}" ]; then
	echo "sed -i 's:<\/runTest>:<\/runTest>\\n:g' $CWD/build.ci.xml" | sh
else
	echo "sed -i 's:<\/runTest>:<\/runTest>\\\\n:g' $CWD/build.ci.xml" | sh
fi
