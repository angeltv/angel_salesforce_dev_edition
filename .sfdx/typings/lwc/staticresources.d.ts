declare module "@salesforce/resourceUrl/BlankAvatar" {
    var BlankAvatar: string;
    export default BlankAvatar;
}
declare module "@salesforce/resourceUrl/GetAnimalResource" {
    var GetAnimalResource: string;
    export default GetAnimalResource;
}
declare module "@salesforce/resourceUrl/Mobile_Design_Templates" {
    var Mobile_Design_Templates: string;
    export default Mobile_Design_Templates;
}
declare module "@salesforce/resourceUrl/SiteSamples" {
    var SiteSamples: string;
    export default SiteSamples;
}
declare module "@salesforce/resourceUrl/countup7" {
    var countup7: string;
    export default countup7;
}
declare module "@salesforce/resourceUrl/heatmap" {
    var heatmap: string;
    export default heatmap;
}
declare module "@salesforce/resourceUrl/heatmapmock" {
    var heatmapmock: string;
    export default heatmapmock;
}
declare module "@salesforce/resourceUrl/jQueryMobile" {
    var jQueryMobile: string;
    export default jQueryMobile;
}
declare module "@salesforce/resourceUrl/jquery" {
    var jquery: string;
    export default jquery;
}
declare module "@salesforce/resourceUrl/leaflet" {
    var leaflet: string;
    export default leaflet;
}
declare module "@salesforce/resourceUrl/leaflet1" {
    var leaflet1: string;
    export default leaflet1;
}
declare module "@salesforce/resourceUrl/nouislider" {
    var nouislider: string;
    export default nouislider;
}
declare module "@salesforce/resourceUrl/path_js" {
    var path_js: string;
    export default path_js;
}
declare module "@salesforce/resourceUrl/underscore_1_5_1" {
    var underscore_1_5_1: string;
    export default underscore_1_5_1;
}
declare module "@salesforce/resourceUrl/vfimagetest" {
    var vfimagetest: string;
    export default vfimagetest;
}
