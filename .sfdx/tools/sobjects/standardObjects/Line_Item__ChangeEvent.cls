// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Line_Item__ChangeEvent {
    global Object ChangeEventHeader;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Id Id;
    global Id Invoice__c;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Decimal Line_Item_Total__c;
    global Merchandise__c Merchandise__c;
    global String Name;
    global Double Quantity__c;
    global String ReplayId;
    global Double Unit_Price__c;
    global String Warehouse_Name__c;
    global String Warehouse__c;

    global Line_Item__ChangeEvent () 
    {
    }
}