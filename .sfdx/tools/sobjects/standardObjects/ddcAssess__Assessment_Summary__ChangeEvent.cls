// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ddcAssess__Assessment_Summary__ChangeEvent {
    global Object ChangeEventHeader;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Id Id;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global SObject Owner;
    global Id OwnerId;
    global String ReplayId;
    global Double ddcAssess__Available_Accounts_By_Industry__c;
    global Double ddcAssess__Available_Contacts_By_Industry__c;
    global Double ddcAssess__Available_Contacts__c;
    global Double ddcAssess__Available_Executive_Contacts_By_Industry__c;
    global String ddcAssess__Batch_Id__c;
    global Boolean ddcAssess__Complete__c;
    global Datetime ddcAssess__Completion_Date__c;
    global String ddcAssess__DUNSRight_Result__c;
    global Boolean ddcAssess__Dunsright__c;
    global Boolean ddcAssess__Dupe_Check__c;
    global String ddcAssess__Employee_Range_Count__c;
    global Double ddcAssess__Error_Count__c;
    global Double ddcAssess__Failed_pcnt__c;
    global String ddcAssess__Industry_Count__c;
    global String ddcAssess__Revenue_Range_Count__c;
    global Double ddcAssess__Total__c;
    global Double ddcAssess__Version__c;

    global ddcAssess__Assessment_Summary__ChangeEvent () 
    {
    }
}